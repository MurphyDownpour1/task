<?php

require_once 'libs/Db.php';

$db = new Db();
$data = $db->getAll();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>List</title>
</head>
<body>
    <div class="text-center">
        <a class="btn btn-primary" href="/create">Create</a>
    </div>
    <?php if (isset($data)): ?>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Date</th>
                <th scope="col">Person</th>
                <th scope="col">Priority</th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col">Comments</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $value): ?>
                <tr>
                    <td><?= $value['date'] ?></td>
                    <td><?= $value['resp_person'] ?></td>
                    <td><?php
                        switch ($value['priority'])
                        {
                            case 0:
                                echo 'Low';
                                break;
                            case 1:
                                echo 'Medium';
                                break;
                            case 2:
                                echo 'High';
                                break;
                        }
                        ?></td>
                    <td><?= $value['title'] ?></td>
                    <td><?= $value['description'] ?></td>
                    <td><?= $value['comments'] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</body>
</html>