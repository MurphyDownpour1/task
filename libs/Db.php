<?php
/**
 * Created by PhpStorm.
 * User: Jarvis
 * Date: 22.11.2018
 * Time: 20:13
 */

class Db
{
    private $connection;

    public function __construct()
    {
        $this->connection = new PDO('mysql:host=localhost;dbname=task', 'root', '');
    }

    public function save($date, $person, $priority, $title, $description, $comments)
    {
        $statement = $this->connection
            ->prepare('INSERT INTO info (`date`, `resp_person`, `priority`, `title`, `description`, `comments`)
                                  VALUES(:date, :person, :priority, :title, :description, :comments)');

        $statement->bindParam(':date', $date);
        $statement->bindParam(':person', $person);
        $statement->bindParam(':priority', $priority);
        $statement->bindParam(':title', $title);
        $statement->bindParam(':description', $description);
        $statement->bindParam(':comments', $comments);

        $result = $statement->execute();

        return $result;
    }

    public function getAll()
    {
        $data = $this->connection->query('SELECT * FROM info')->fetchAll(2);

        return $data;
    }
}