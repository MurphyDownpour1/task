<?php

class FormValidator
{
    private $date;
    private $person;
    private $priority;
    private $title;
    private $description;
    private $comments;

    public function __construct($date, $person, $priority, $title, $description, $comments)
    {
        $this->date = $date;
        $this->person = $person;
        $this->priority = $priority;
        $this->title = $title;
        $this->description = $description;
        $this->comments = $comments;
    }

    public function init()
    {
        $validation1 = $this->validateDate();
        $validation2 = $this->validatePriority();
        $validation3 = $this->validateString();
        $validation4 = $this->validateStringLength();

        if ($validation1 && $validation2 && $validation3 && $validation4)
            return true;
        else
            return false;
    }

    private function validateDate()
    {
        if (DateTime::createFromFormat('Y-m-d', $this->date) !== false)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private function validateString()
    {
        if (is_string($this->person)
            && is_string($this->description)
            && is_string($this->title)
            && is_string($this->comments))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private function validateStringLength()
    {
        if (
            ( strlen($this->description) <= 255 && strlen($this->comments) <= 255 )
                            &&
            ( strlen($this->title) <= 100 && strlen($this->person) <= 100 )
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private function validatePriority()
    {
        if (is_numeric($this->priority))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}