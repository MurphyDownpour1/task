<?php

$uri = $_SERVER['REQUEST_URI'];

$slug = substr($uri, 1);

switch ($slug)
{
    case 'list':
        ob_start();
        include 'list.php';
        $page = ob_get_contents();
        ob_end_clean();
        echo $page;
        break;
    case 'create':
        ob_start();
        include 'create.php';
        $page = ob_get_contents();
        ob_end_clean();
        echo $page;
        break;
}