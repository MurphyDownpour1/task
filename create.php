<?php

require_once 'libs/FormValidator.php';
require_once 'libs/Db.php';

$message = "";
$status = "";

$date = $_POST['date'];
$person = $_POST['person'];
$priority = $_POST['priority'];
$title = $_POST['title'];
$description = $_POST['description'];
$comments = $_POST['comments'];

if (isset($date)
    && isset($person)
    && isset($priority)
    && isset($title)
    && isset($description)
    && isset($comments))
{
    $validator = new FormValidator($date, $person, $priority, $title, $description, $comments);
    $result = $validator->init();

    if (!$result)
    {
        $message = "Please, enter valid data.";
        $status = "fail";
    }
    else
    {
        $db = new Db();
        $result = $db->save($date, $person, $priority, $title, $description, $comments);

        if ($result)
        {
            header('Location: list.php');
        }
        else
        {
            $message = "Failed to save to DB.";
            $status = "fail";
        }
    }
}
else
{
    $message = "All fields are required. Fill all blanks.";
    $status = "fail";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/create.css">
    <title>Create</title>
</head>
<body>
    <form action="" method="POST">
        <div id="message">
            <h4 class="<?= $status ?>"><?= $message ?></h4>
        </div>
        <div class="form-control">
            <label for="">Date</label>
            <input type="date" name="date">
        </div>

        <div class="form-control">
            <label for="">Responsible Person</label>
            <input type="text" name="person">
        </div>

        <div class="form-control">
            <label for="">Priority</label>
            <select name="priority" id="">
                <option value="0">Low</option>
                <option value="1">Medium</option>
                <option value="2">High</option>
            </select>
        </div>

        <div class="form-control">
            <label for="">Title</label>
            <input type="text" name="title">
        </div>

        <div class="form-control">
            <label for="">Description</label>
            <span>(maximum length is 255 characters)</span>
            <textarea name="description" id="" cols="30" rows="10"></textarea>
        </div>

        <div class="form-control">
            <label for="">Comments</label>
            <span>(maximum length is 255 characters)</span>
            <textarea name="comments" id="" cols="30" rows="10"></textarea>
        </div>

        <input type="submit">
    </form>
</body>
</html>